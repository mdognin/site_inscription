<!DOCTYPE html>
<html>
<?php
		//definition des paramètres du pélé : dates, prix, etc
		include 'script/definition_pele.php';
?>
	<head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="../style/style.css" />
        <?php
		echo '<title>'.$sujet.'</title>';
		?>
        <link rel="icon" type="image/png" href="../style/favicon.png" />
    </head>
    
    <body>

    	<header>
    		<a href="https://www.hospitalite30.fr/">
			<img src="../style/Logo_hospitalite.png" class="logo" alt="logo_hospitalite_st_jean_Paul_II" />
			</a>
			<?php
			echo '<div id="entete">'.$titre.'</div>';
			?>
    	</header>

<?php
//sécurisation des champs du formulaire (retrait des balises php / SQL)
include '../texte/secure.php';
//Supprimer les erreur avant le calcul de prix
include 'script/suppression_erreurs.php';

$mail = $_POST['adresse_mail']; 

//s'il n'y a pas de paiement on redirige pas vers la plateforme de paiement
if (isset($_POST['couple_paiement_fait']) and !in_array($_POST['hotel'],$liste_hotel_paiement_online))
{
	echo '<form method="POST"	action="../retour_boutique.php">"';
}
else
{
	echo '<form method="POST"	action="https://paiement.systempay.fr/vads-payment/">"';
}
?>

<article>

<?php

//validation des données
include 'script/validation.php' ;

if ($validation == "NOK")
{
	$compteur="000NOK";
}

//si toutes les conditions sont remplies les données peuvent être transmises à la base de données
if ($validation == "OK")
{

	//Identifier le numéro de transaction lié à cette inscription
	include '../texte/compteur_transaction.php';

	//calculer le prix de l'inscription
	include 'script/calcul_prix.php';

	//paramètres transaction
	include 'script/transaction.php' ;

	


	
	echo '<div class="part"><p class="bloc_note">'.'Après le paiement vous recevrez une confirmation d\'inscription par email.<br>Si vous ne recevez pas ce mail veuillez vérifier vos SPAM.<br>Sans paiement en ligne nous ne pourrons pas la prendre en compte.'.'</p>';

	echo "<p class='centre'>"."Pour toute question concernant votre inscription merci de nous contacter à cette adresse mail : hospitalite30.catholique@gmail.com "."</p>"."<br>"."</div>";

	echo "<h2>Veuillez vérifier que les informations de votre inscritpion sont correctes</h2>";

		
	
	//écriture message avant la confirmation
	$passage_ligne = "<br>";
	
	include 'script/message_recapitulatif.php' ;

	if ($_POST['hotel']!= "hebergement_personnel" and !isset($_POST['categorie_promo_jeune']) )
	{
		$message_html .= "- Je règlerai le prix de l'hébergement directement auprès de l'hotel".$passage_ligne;
	}

	if (isset($_POST['couple_paiement_fait']) and !in_array($_POST['hotel'],$liste_hotel_paiement_online))
	{
		$vads_order_info2 = $compteur."_".$_POST['nom']."_".$_POST['prenom'];
		
	}

	if ($paiement_effectue=1)
		{
			$temp=strlen($paiement_prix_inscription);
			$part1=substr($paiement_prix_inscription,0,$temp-2);
			$part2=substr($paiement_prix_inscription,-2);
			$message_html .= "- Je dois régler en ligne la somme de : ".$part1.",".$part2." €".$passage_ligne;
		}

	echo $message_html;

	//déclaration passage de ligne pour les mails
	$passage_ligne = "\n";
	include 'script/message_recapitulatif.php';
	//création du fichier texte qui sauvegarde le contenu du mail

	$nom_fichier = "mail/".$vads_order_info2."_mail.txt";
	$fp = fopen ($nom_fichier, "w+");
	fseek ($fp, 0);
	fputs ($fp, $message_html);
	fclose ($fp);

	//Insérer les informations de l'inscription dans la BDD
	include '../texte/preparation_requete_bdd.php';

	include '../texte/insertion_bdd.php';

}

?>

<input type="hidden" name="vads_action_mode" value= "<?php echo $vads_action_mode;	?>"/>
<input type="hidden" name="vads_amount" value="<?php echo $vads_amount;	?>" />
<input type="hidden" name="vads_ctx_mode" value="<?php echo $vads_ctx_mode;	?>" />
<input type="hidden" name="vads_currency" value="<?php echo $vads_currency;	?>" />
<input type="hidden" name="vads_cust_email" value="<?php echo $vads_cust_email;	?>" />
<input type="hidden" name="vads_cust_first_name" value="<?php echo $vads_cust_first_name;	?>" />
<input type="hidden" name="vads_cust_last_name" value="<?php echo $vads_cust_last_name;	?>" />
<input type="hidden" name="vads_cust_phone" value="<?php echo $vads_cust_phone;	?>" />
<input type="hidden" name="vads_order_info" value="<?php echo $vads_order_info;	?>" />
<input type="hidden" name="vads_order_info2" value="<?php echo $vads_order_info2;	?>" />
<input type="hidden" name="vads_order_info3" value="<?php echo $vads_order_info3;	?>" />
<input type="hidden" name="vads_page_action" value="<?php echo $vads_page_action;	?>" />
<input type="hidden" name="vads_payment_config" value="<?php echo $vads_payment_config;	?>" />
<input type="hidden" name="vads_redirect_success_message" value= "<?php echo $vads_redirect_success_message;  ?>"  />
<input type="hidden" name="vads_redirect_success_timeout" value= "<?php echo $vads_redirect_success_timeout;  ?>"  />
<input type="hidden" name="vads_return_mode" value= "<?php echo $vads_return_mode;  ?>"  />
<input type="hidden" name="vads_site_id" value="<?php echo $vads_site_id;	?>" />
<input type="hidden" name="vads_trans_date" value= "<?php echo $vads_trans_date; ?>" />
<input type="hidden" name="vads_trans_id" value="<?php echo $vads_trans_id;	?>" />
<input type="hidden" name="vads_user_info" value="<?php echo $vads_user_info;	?>" />
<input type="hidden" name="vads_validation_mode" value="<?php echo $vads_validation_mode;	?>" />
<input type="hidden" name="vads_version" value="<?php echo $vads_version;	?>" />
<input type="hidden" name="signature" value= "<?php echo $signature;  ?>"  />


<div id="bouton">

<?php

$mail = $_POST['adresse_mail']; 

//s'il n'y a pas de paiement on redirige pas vers la plateforme de paiement
if (isset($_POST['couple_paiement_fait']) and !in_array($_POST['hotel'],$liste_hotel_paiement_online))
{
	echo '<input type="hidden" name="couple_paiement_fait" value= "'. $_POST['couple_paiement_fait'].'" />';
	echo '<input type="hidden" name="adresse_mail" value= "'. $_POST['adresse_mail'].'" />';
	echo '<input type="hidden" name="compteur" value= "'. $compteur.'" />';
	echo '<input type="hidden" name="vads_trans_status" value= "paiement_couple" />';
	

	echo '<input type="submit" name="terminer" value="Terminer"/>'.'</div>';
}
else
{
	echo '<input type="submit" name="payer" value="Payer"/>'.'</div>';
}
?>

</div>


</article>
 </form>
</body>


 </html>