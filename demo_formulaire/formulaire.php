<!DOCTYPE html>
<html>
<?php //definition des paramètres du pélé : dates, prix, etc
		include 'script/definition_pele.php';?>
	<head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="../style/style.css" />
        <?php echo '<title>'.$sujet.'</title>';	?>
        <link rel="icon" type="image/png" href="../style/favicon.png" />
    </head>
	<header>
		<a href="https://www.hospitalite30.fr/">
		<img src="../style/Logo_hospitalite.png" class="logo" alt="logo_hospitalite_st_jean_Paul_II" /> </a>
		<?php echo '<div id="entete">'.$titre.'</div>';?>
	</header>
    <body>

    <input type="hidden" id="navigateur" name="navigateur" value=  "<?php echo $navigateur;	?>"/>

	<form name="general" method="post" action="formulaire_page2.php" onsubmit="return verif_form(this)">

	<article>

	<input type="hidden" id="date_depart" value=  "<?php echo $date_depart;	?>"/>
	
		<!-- RUBRIQUE IDENTITE-->

		<?php
		include '../entete_navigateur.php';
		?>
	<input type="hidden" id="navigateur" name="navigateur" value=  "<?php echo $navigateur;	?>"/>

	<div class="bloc_note">

		<h3> Note : </h3>

		<p><li class="indent" >NOUVEAUTE : Si vous n’êtes pas à jour de votre cotisation annuelle, il vous est possible de vous en acquitter pendant votre inscription au pèlerinage.
		Pour rappel, il est OBLIGATOIRE d'être à jour de cotisation pour participer au pèlerinage </li>
		
		<li class="indent" >Merci d'utiliser un autre navigateur que "Internet Explorer" (Firefox, Safari, Edge, Chrome, Opera etc fonctionnent très bien) </li>

		<li class="indent" >En cas de problème / question, merci d'envoyer un email à hospitalite30.catholique@gmail.com en précisant votre numéro de téléphone, nous vous recontacterons au plus vite</li></p>

	</div>



		<h2>Partie 1 : Qui êtes-vous ?</h2>
	<div class="part">

		<p>	Nom <input type="text" name="nom" value="TEST_Nom" required/>	</p>

		<p> Prénom <input type="text" name="prenom" value="TEST_prenom" required/> 	</p>

		<p>		
		<label for="sexe">Sexe</label>
	    <select name="sexe" id="sexe" required >
	    	<option value="" ></option>
	        <option value="homme" selected="selected">Homme</option>
	        <option value="femme">Femme</option>
	    </select>
		</p>

		<p>	Date de naissance <input type="date" name ="date_naissance" min="1900-01-01" value="1985-10-10" required/>	</p>
				
 		<p>	Téléphone principal (Portable) <input type="tel" name="portable" value="0605040302" required/>	</p>

		<p>	Téléphone fixe (facultatif) <input type="tel" name="telephone" placeholder="04XXXXXXXX" />	</p> 

		<p>	Adresse mail <input type="email" name="adresse_mail" required/>	</p>

		<div id="adresse">
			<p>	Numéro et nom de rue <input type="text" name="rue" value="TEST 5 Rue de la République" required/>	</p>
			<p>	Code postal <input type="text" name="code_postal" value="TEST 69000" required/>	</p>
			<p>	Ville <input type="text" name="ville" value="TEST Lyon" required/>	</p>
		</div>

		<p>	Profession <input type="text" name="profession" value="Testeur"/>	</p>
		</div>
		<!-- RUBRIQUE COTISATION-->
	
		<h2 >Partie 2 : Cotisation annuelle</h2>

		<div class="part">
		<div class="bloc_note">
			Les stagiaires, c'est à dire les personnes venant pour la première fois avec notre Hospitalité sont exemptées de cotisation
		</div>

		<label  for="cotisation">Cotisation : Êtes-vous à jour de cotisation pour l'année 2021 ?</label>
	    <select name="cotisation" id="cotisation" required >
	    	<option value="" selected="selected"></option>
	        <option value="cotisation_a_jour">Oui</option>
	        <option value="cotisation_a_regler">Non</option>
	        <option value="cotisation_stagiaire">Exempté (Stagiaire)</option>
	    </select>
	
	    <div id="id_cotisation_type" style="display:none" >
	    	<label for="cotisation_type">Quel type de cotisation ?</label>
		    <select name="cotisation_type" id="cotisation_type">
		    	<option value="" selected="selected"></option>
		        <option value="cotisation_etud">Cotisation étudiant non salarié</option>
		        <option value="cotisation_solo">Cotisation 1 personne</option>
		        <option value="cotisation_foyer">Cotisation pour les personnes d'un foyer</option>
		    </select>
		</div>
	</br>

		<div id="id_note_2021" class="bloc_note" style="display:none">
		Spécial 2021 : Si vous souhaitez bénéficier du tarif réduit SOLIDAIRE merci de régler votre cotisation en suivant le lien ci dessus. Vous pourrez ensuite effectuer votre inscription en indiquant que vous avez cotisé
		<a href="https://inscription.hospitalite30.fr/cotisation_formulaire/formulaire.php">Règlement cotisation</a>
		</div>
	
		<p id="id_cotisation_foyer" style="display:none" >
		Prenom des autres personnes du foyer couvertes par la cotisation <input type="text" name="cotisation_foyer_liste"/>
		</p>	

		<p  id="id_cotisation_abo" style="display:none" >
		<span> Si vous ne souhaitez pas souscrire l'abonnement au journal de l'hospitalité SERVIR veuillez cocher la case ci dessous :</span>
		<input type="checkbox" name="refus_abonnement_servir" value="refus_abonnement_servir" id="refus_abonnement_servir" /> 
		</p>
		</div>
		</div>
		
		<!-- RUBRIQUE PARTICIPATION-->
	
		<h2>Partie 3 : Vos précédentes participations</h2>

		<div class="part">

		<p>
		<label for="participation">Venez-vous pour la première fois ?</label><br />
	  
	    <select name="participation" id="participation"  required>
	    	<option value="" selected="selected"></option>
	        <option value="participation_oui" >Oui</option>
	        <option value="participation_non" onclick='effacer("parrain_nom","parrain_prenom")'>Non</option>
	    </select>
		

		<div id="id_participation_oui" style="display:none" >
			<p>Personne qui vous parraine (si vous connaissez quelqu'un) :</p>
			<p>Prénom <input type="text" name="parrain_prenom"/>	</p>
			<p>Nom <input type="text" name="parrain_nom"/>	</p>
			
		</div>		

		

		<div id="id_participation_non" style="display:none" >
			<p>Participation aux précédents pèlerinages :</p>
			<p>		
			<label for="participation_engagement">Avez-vous réalisé votre engagement ?</label><br />
	   		<select name="participation_engagement" id="participation_engagement" >
	   			<option value="" selected="selected"></option>
	        	<option value="engagement_oui">Oui</option>
	        	<option value="engagement_non" onclick='effacer("","participation_annees")'>Non</option>
	    	</select>
			</p>
			<p>Si non, merci d'indiquer les années de vos précédents pèlerinages avec notre hospitalité : <input type="text" name="participation_annees"/>	</p>
		</div>
		</div>

		<!-- <textarea name="rmq_participation" rows="4" cols="30" placeholder="Remarque Participation"></textarea> -->
		
		<!-- RUBRIQUE TRANSPORT-->
		
		<h2>Partie 4 : Transport</h2>

		<div class="part">
		
		<p>
		<label for="bus">Prenez-vous le bus ? </label><br />
	    <select name="bus" id="bus" required>
	    	<option value="" selected="selected"></option>
	        <option value="bus_oui" >Oui</option>
	        <option value="bus_non" >Non</option>
	    </select>
		</p>

		<div id="id_bus_oui" style="display:none" >
			<p>		
			<label for="bus_depart">D'où voulez vous partir ?</label><br />
	   		<select name="bus_depart" id="bus_depart" >
	   			<option value="" selected="selected"></option>
	        	<option value="bus_nimes">Nîmes</option>
	        	<option value="bus_le_vigan">Le Vigan</option>
	        	<option value="bus_ales">Alès</option>
	        	<option value="bus_bagnols">Bagnols-sur-Cèze</option>
	        	<option value="bus_villeneuve">Villeneuve-lès-Avignon</option>
	    	</select>
			</p>

			<div class="bloc_note">
				Le bus de Villeneuve sera maintenu que si un minimum de personnes le sélectionne. Vous serez informé en avance s'il est annulé
			</div>
				
			<p>		
			<label for="categorie">Faites-vous partis d'un des groupes suivants ? (COUPLE, ENFANT, ADO, PROMO JEUNE)<br>
			SI NON, sélectionner "NON"</label><br />
	   		<select name="categorie" id="categorie" >
		   		<option value="" ></option>
		       	<option value="categorie_non" selected="selected">Non</option>
		   		<option value="categorie_enfant">Enfant (moins de 10 ans le jour du départ)</option> 
		       	<option value="categorie_ado">Ado (de 10 à 16 ans inclus le jour du départ)</option>
		       	<option value="categorie_couple">Je voyage en couple dans le bus</option>
		       	<option value="categorie_jeune">Jeune ETUDIANT ou SANS EMPLOI (de 17 à 25 ans inclus le jour du départ)</option>
	    	</select>
			</p>
		</div>

		<p id="bloc_alerte_age" class="alerte" style="display:none"> Votre age ne correspond pas à la catégorie choisie. Si vous pensez qu'il y a une erreur entrez votre date de naissance sous le format AAAA-MM-JJ</p>

		<div id="id_bus_non" style="display:none" >
			<p>Une participation de 15€ est demandée pour les personnes venant par leur propre moyen (assurance et cotisation à reverser aux sanctuaires)</p>
			<p>		
		</div>


		<div id="id_categorie_jeune" style="display:none" >
			<p>Cette année l'hébergement PROMO JEUNES sera réalisé à l'hotel en formule éco.</p>
			<p>
			<input type="checkbox" name="categorie_promo_jeune" id="categorie_promo_jeune" value="categorie_promo_jeune"/>
				<label for="categorie_promo_jeune">
				Je souhaite profiter de la promo jeune
				</label>
			</p>
		</div>

		<div class="bloc_couleur_1" id="id_categorie_couple" style="display:none" >
			<p>Nom et Prénom du/de la conjoint/e :</p>
			<p>Prénom : <input type="text" name="couple_prenom" />	</p>
			<p>Nom : <input type="text" name="couple_nom" />	</p>
			<p>Concernant les couples, il faut réaliser 2 inscriptions séparées. Cependant le règlement du transport ne s'effectue que pour une des 2 personnes </p>
			<p>
			<input type="checkbox" name="couple_paiement_fait" id="couple_paiement_fait" value="couple_paiement_fait" />
				<label for="couple_paiement_fait">
				Le paiement du transport pour mon couple a déjà été fait par ma conjointe / mon conjoint
				</label>
			</p>
			<p>
			<label for="couple_tarif">Tarif couple : Quelle formule choisissez-vous ? </label><br />
	    		<select name="couple_tarif" id="couple_tarif" >
	    		<option value="" selected="selected"></option>
	        	<option value="couple_tarif_soutien" >Tarif couple</option>
	        	<option value="couple_tarif_reduit" >Tarif réduit couple</option>
	   		</select>
			</p>
		</div>

		<textarea  name="rmq_transport" rows="4" cols="30" placeholder="Remarque transport"></textarea>
		</div>

	<!--RUBRIQUE HEBERGEMENT-->
	<h2>Partie 5 : Hébergement</h2>
	<div class="part">

	
	<div class="bloc_note">
	<p> Le prix des hôtels est présenté en pension complète, par personne et par jour</p>
	<p>Le paiement de l'hôtel se fait directement sur place auprès de l'hôtelier, à l'exception de l'Ave Maria, <!--St Michel, -->St Frai qui sont encaissés par l'hospitalité</p>
		<table>
		<tr><th> Hôtels </th><th> Chambre individuelle </th><th> Chambre 2 lits </th><th> Chambre 3 lits </th></tr>
		<tr><td> Saint-Frai </td><td colspan=3> 45,00 €</td></tr>
		<tr><td> Ave-Maria </td><td> 35,75 € </td><td> 29,10 € </td><td> Non Dispo </td></tr>
		<!--<tr><td rowspan=2> Ave-Maria </td><td> 35,75 € </td><td> 29,10 € </td><td> Non Dispo </td></tr>
		<tr><td colspan=3> Cet Hôtel est réservé en priorité aux hospitalières bénéficiant du forfait jeunes </td></tr>-->
		<!--<tr><td> Saint-Michel </td><td colspan=3> 22,50 € Dortoir réservé aux garçons bénéficiant du forfait jeunes </td></tr>-->
		<!--<tr><td> Printania </td><td> 61,50 € </td><td> 41,50 € </td><td> Non Dispo </td></tr>-->
		<tr><td> Saint-Agnès </td><td> 61,00 € </td><td> 49,00 € </td><td> Non Dispo </td></tr>
		<!--<tr><td> Ariane </td><td> 56,50 € </td><td> 38,50 € </td><td> 38,50 </td></tr>-->
		<tr><td> National </td><td> 72,50 € </td><td> 47,50 € </td><td> 47,50 € </td></tr>
		<tr><td> Croix des bretons </td><td> 72,50 € </td><td> 47,50 € </td><td> 47,50 € </td></tr>
		<tr><td> Stella Matutina </td><td> 74,00 € </td><td> 49,00 € </td><td> 49,00 € </td></tr>
		<tr><th colspan=4> FORMULE ECO </td></tr>
		<tr><td> Croix des Bretons </td><td> 67,50 € </td><td> 42,50 € </td><td> 42,50 € </td></tr>
		<tr><td> National </td><td> 64,50 € </td><td> 41,50 € </td><td> 41,50 € </td></tr>
		<tr><td> Stella Matutina </td><td> 65,50 € </td><td> 40,50 € </td><td> 40,50 € </td></tr>
		</table>
		<br />

	<span class="gras"> Conditions Formule Eco : <br />
	- Apporter le linge de toilette<br />
	- Pas de ménage<br />
	- Petit déjeuner complet<br />
	- Déjeuner et diner 1 plat (à volonté) + 1 dessert<br /> 
	</span>
	</div>

	<div id="id_hebergement" style="display:block" >
				
		<p>		
		<label for="hotel">Quel hôtel choisissez-vous ?</label><br />
	   	<select name="hotel" id="hotel" required>
	   		<option value="" selected="selected"></option>
	       	<!--<option value="hotel_saint_frai">Saint Frai</option>-->
	       	<!--<option value="hotel_ave_maria">Ave Maria</option>-->
	     
	       	<option value="hotel_sainte_agnes">Sainte Agnès</option>
	       	<option value="hotel_national">National</option>
	      	<option value="hotel_croix_des_bretons">Croix des Bretons</option>
	       	<option value="hotel_stella_matutina">Stella Matutina</option>
	        <option value="hotel_croix_des_bretons_eco" >Formule ECO : Croix des Bretons ECO</option>
	        <option value="hotel_national_eco" >Formule ECO : National ECO</option>
	        <option value="hotel_stella_matutina_eco" >Formule ECO : Stella Matutina ECO</option>
	        <option value="hebergement_perso">Hébergement personnel</option>
	   			       	
	    </select>
		</p>
	</div>
		

		<p id="id_hotel_ch_double" style="display:none" >
		
			<label for="hotel_chambre_12">Type de chambre souhaitée : </label><br />
	    		<select name="hotel_chambre_12" id="hotel_chambre_12" >
	    		<option value="" selected="selected"></option>
	        	<option value="hotel_simple" >Chambre simple</option>
	        	<option value="hotel_double" >Chambre double</option>
	   		</select>
		</p>
		
		<p id="id_hotel_ch_triple" style="display:none" >
			
			<label for="hotel_chambre_123">Type de chambre souhaitée : </label><br />
	    		<select name="hotel_chambre_123" id="hotel_chambre_123" >
	    		<option value="" selected="selected"></option>
	        	<option value="hotel_simple" >Chambre simple</option>
	        	<option value="hotel_double" >Chambre double</option>
	        	<option value="hotel_triple" >Chambre triple</option>
	   		</select>
		</p>
	

		<p id="bloc_alerte_chambre" class="bloc_noir" style="display:none">

		<span  class="alerte">Merci de remplir correctement cette partie, elle est importante pour attribuer les chambre correctement</span>
		
		
		<br><br>Si vous souhaitez partager la chambre avec d'autres personnes merci de renseigner leur nom
		<br><br>Personne 1 :
		<br>Prénom : <input class="fond_rouge" type="text" name="chambre_prenom1" value="TEST_chambre_prenom1" /> Nom : <input class="fond_rouge" type="text" name="chambre_nom1" value="TEST_chambre_nom1"/>
		<br><br><span id="personne2">Personne 2 :
		<br>Prénom : <input class="fond_rouge" type="text" name="chambre_prenom2" value="TEST_chambre_prenom2" /> Nom : <input class="fond_rouge" type="text" name="chambre_nom2" value="TEST_chambre_nom2"/></span>

		<br class="italique">Si vous ne savez pas avec qui partager la chambre, veuillez cocher la case ci contre : 
		<input type="checkbox" name="chambre_repartition" value="chambre_repartition" id="chambre_repartition" /> <label for="chambre_repartition"> </label>
		<br > ==> Nous complèterons la chambre avec un/une autre hospitalier </br>

		</p>

		<textarea id="rmq" name="rmq_hebergement" rows="4" cols="30" placeholder="Remarque hébergement"></textarea>
		</div>

		
		<!--RUBRIQUE SERVICE-->
		
		<h2 id="cible_test">Partie 6 : Service</h2>

		<div class="part">

							<!--Service Médical-->
		<p>		
		<label for="equipe_med">Avez-vous des compétences médicales ?</label><br />
	   	<select name="equipe_med" id="equipe_med" >
	   		<option value="" selected="selected">Non</option>
	       	<option value="medecin">Médecin</option>
	       	<option value="brancardier">Brancardier(e)</option>
	       	<option value="infirmier">Infirmier(e)</option>
	       	<option value="kine">Kinésithérapeute</option>
	       	<option value="pharma">Pharmacien(ne)</option>
	       	<option value="dentiste">Dentiste</option>
	   	</select>
		</p>

		<p>Où souhaitez-vous effectuer votre service ? <br />
		Vous devez impérativement cocher une des cases ci dessous<br />

			<input id="service_unite" type="checkbox" name="service" value="service_unite" /> <label for="service_unite">Dans une unité de chambres de malades</label><br />

			<input type="checkbox" name="service" value="service_marchants" id="service_marchants" /> <label for="service_marchants">Dans le groupe des « marchants »</label><br />
			
			<input type="checkbox" name="service" value="service_pages" id="service_pages" /> <label for="service_pages">Encadrement des pages</label><br />
			<input type="checkbox" name="service" value="service_cour" id="service_cour" /> <label for="service_cour">Dans le groupe de la « COUR » (transport des malades et du matériel)</label><br />
			<input type="checkbox" name="service" value="service_manger" id="service_manger" /> <label for="service_manger_intendance">Dans le groupe « Salle à manger » et service Intendance</label><br />
			<input type="checkbox" name="service" value="service_desinfection" id="service_ecoute" /> <label for="service_ecoute">Au service nettoyage, désinfection</label><br />
			<input type="checkbox" name="service" value="service_musique" id="service_musique" /> <label for="service_musique">Dans le groupe des musiciens</label><br />
			<input type="checkbox" name="service" value="service_piscine_" id="service_piscine_"/> <label for="service_piscine_">Piscine (geste de l’eau, service de journée)</label><br />
			<input type="checkbox" name="service" value="non_renseigne" id="non_renseigne"/> <label for="non_renseigne">Autre</label><br />
		</p>
		<div class="bloc_note">L'affectation définitive des hospitaliers est tributaire des besoins du pélerinage</div>

		<p>Nuit / Demi nuit de garde (facultatif) :<br />
		<input type="checkbox" name="service_garde" value="service_garde_entiere" id="service_garde_entiere" /> 
		<label for="service_garde_entiere">J'accepte d'assurer une nuit de garde entière</label><br />
		<input type="checkbox" name="service_garde" value="service_garde_demi" id="service_garde_demi" /> 
		<label for="service_garde_demi">J'accepte d'assurer une demi nuit de garde (21h-1h30 ou 1h30-6h)</label><br />
		<br />	

		<div id="id_service_unite_toilette" style="display:none">
		<input type="checkbox" name="service_unite_toilette" id="service_unite_toilette" value="service_unite_toilette"/>
		<label for="service_unite_toilette">J'accepte d'aider des personnes malades à la toilette ?</label>
		</div>
		</div>
	
		<!--FINALISATION-->
		
		<h2>Partie 7 : Prise de Vue</h2>

		<div class="part">

		<p>Prise de vue : Est-ce que vous nous autorisez à utiliser des photos / vidéos sur lesquelles vous apparaissez, ceci sur différents supports (écrit, audio-visuel) ?
		Reconnaissez-vous avoir pris connaissance de l’utilisation qui en est faite dans le cadre exclusif de l’Hospitalité St Jean-Paul II</p>
		<input type="checkbox" name="prise_de_vue" id="prise_de_vue" value="prise_de_vue"  required/>
		<label for="prise_de_vue">J’autorise et je reconnais les points évoqués pour la prise de vue</label>

		<br />
		<br />
		</div>



		<h2>Partie 8 : Protocole Sanitaire</h2>

		<div class="part">
		
		<input type="checkbox" name="protocole" id="protocole" value="protocole" required/>
		<label for="prise_de_vue">En cochant cette case, je certifie avoir lu intégralement le protocole sanitaire et je m'engage à le respecter rigoureusement.</label><a href="Synthese_protocole_interne_Juillet_2021.pdf">Protocole disponible ici</a>

		</div>

		<div id="bouton">
			
		<input type="submit" name="validation" value="Valider">
		 <input type="reset" name="nom" value="Remise à zéro">
		</div>
	
	</article>
	</form>

	<script type="text/javascript" src="../style/validation_formulaire.js"></script>
	<script type="text/javascript" src="../style/traitement_formulaire.js"></script>
	<script type="text/javascript" src="../style/ecoute_formulaire.js"></script>

	</body>

</html>
