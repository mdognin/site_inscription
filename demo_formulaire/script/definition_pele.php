<?php
//Pour les paiements différé, inscrire la date d'encaissement
//$date_encaissement="05/01/2020";
//définition date de départ
$date_depart="05/07/2021";
date_default_timezone_set('Europe/Paris');
$heure_inscription = "";
$heure_inscription .= date('l d/m/Y - H:i:s');
$date_inscription = date('d/m/Y');
echo "Date d'inscription : " .$heure_inscription ."<br>";

//catalogue des prix
$index_prix = 0;
$autorisation_paiement = 0;
$paiement_montant_regle_online=0;
$paiement_prix_inscription=0;

$prix_abo_servir=2000;
$prix_cotisation_etud=2500;
$prix_cotisation_solo=1500;
$prix_cotisation_foyer=1000;
$prix_enfant=2000;
$prix_ado=4500;
$prix_jeune=13000;
$prix_normal=9000;
$prix_couple_soutien=18000;
$prix_couple_reduit=13500;
$prix_non_bus=1500;
$prix_saint_frai=4500*4;
$prix_ave_maria_ch1=3575*4;
$prix_ave_maria_ch23=2910*4;
$prix_cotisation_ave_maria=300;
$prix_saint_michel=2250*4;

//MESSAGE RECAPITULATIF
$objet_inscrption="Vous venez de vous inscrire au pèlerinage de Juillet 2021 avec l'Hospitalité saint Jean-Paul II ";
$sujet = "Pèlerinage Juillet 2021";
$titre = "Formulaire d'inscription pour le pèlerinage Lourdes Juillet 2021";

//BDD
$nom_bdd='pele_juillet_2021';

//TEST ou PRODUCTION
$vads_ctx_mode = 'TEST';

//Transaction
$vads_order_info="PELE_JU : ";
$vads_order_info3 = "pelerinage";

//HEBERGEMENT PROMO JEUNE
if (isset($_POST['sexe']))
{
if ($_POST['sexe']=="homme" and isset($_POST['categorie_promo_jeune']))
{
	$_POST['hotel']="hebergement_PROMO_jeune_HOMME";
}
if ($_POST['sexe']=="femme" and isset($_POST['categorie_promo_jeune']))
{
	$_POST['hotel']="hotel_ave_maria";
}
}
//PENSER à MAJ LE FICHIER ecoute_formulaire.js POUR LA LISTE DES HOTELS
//définition des hotels chambre double, triple
//définition des hotels avec chambre individuel et ch double
$liste_hotel_ch_double = ['hotel_sainte_agnes','hotel_ave_maria','hotel_saint_frai'];
//définition des hotels avec chambre ind, ch double, ch triple
$liste_hotel_ch_triple = ['hotel_national','hotel_stella_matutina','hotel_croix_des_bretons','hotel_croix_des_bretons_eco','hotel_stella_matutina_eco','hotel_national_eco','hotel_saint_frai','hotel_ariane'];
$liste_hotel_paiement_online = ['hotel_ave_maria','hotel_saint_frai'];

// AVEC ST MICHEL $liste_hotel_paiement_online = ['hotel_ave_maria','hotel_saint_frai','hotel_saint_michel'];

//PENSER A DEFINIR LE SUJET DU MAIL DANS RETOUR BOUTIQUE

//Définition des variables BDD
$liste_variable_bdd= ['nom','prenom','compteur','sexe','date_naissance','age','portable','telephone','adresse_mail','rue','code_postal','ville','profession','rmq_identite','participation','parrain_prenom','parrain_nom','participation_engagement','participation_annees','bus','bus_depart','categorie','categorie_promo_jeune','couple_prenom','couple_nom','couple_paiement_fait','couple_tarif','rmq_transport','hotel','hotel_chambre_12','hotel_chambre_123','chambre_prenom1','chambre_nom1','chambre_prenom2','chambre_nom2','cotisation_ave_maria','nom_ave_maria','rmq_hebergement','equipe_med','service','service_piscine','service_piscine_','service_unite_toilette','service_garde','prise_de_vue','heure_inscription','statut_paiement','details_paiement','cotisation_bdd','abo_servir','protocole','navigateur']; //'hotel_bis','hotel_chambre_12_bis','hotel_chambre_123_bis',

?>