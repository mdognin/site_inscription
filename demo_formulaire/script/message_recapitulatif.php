

<?php
$message_html =" ";
if ($passage_ligne == "<br>")
{ 	$message_html .="<div class=\"part\">"; }

//=====Déclaration des messages au format texte et au format HTML.

$message_html .= "Bonjour ".$_POST['prenom'] ." ".$_POST['nom'].$passage_ligne.$passage_ligne.
$objet_inscrption.$passage_ligne.
"Voici un récapitulatif de votre inscription : ".$passage_ligne.$passage_ligne;

if ($passage_ligne == "<br>")
{ 	$message_html .="</div>"."<div class=\"part\"><h2> IDENTITE </h2>".$passage_ligne; }
else
{ 
	$message_html .=" -----------".$passage_ligne.
	"/ IDENTITE /".$passage_ligne.
	"-----------".$passage_ligne;
}

$message_html .= "- Nom : ". $_POST['nom'].$passage_ligne. 
"- Prenom : ". $_POST['prenom'].$passage_ligne."- ".
$_POST['sexe'].$passage_ligne.
"- Date de naissance et âge : " .$_POST['date_naissance'].", ".$age." ans".$passage_ligne;



//SUITE IDENTITE
$message_html .="- Mobile : ".$_POST['portable'].$passage_ligne;

if (isset($_POST['telephone']))
{
	$message_html .="- Tel. fixe : ".$_POST['telephone'].$passage_ligne;
}
$message_html .="- Adresse mail : ". $_POST['adresse_mail'].$passage_ligne.
"- Adresse : ".$_POST['rue'].", ". $_POST['code_postal']." ".$_POST['ville'].$passage_ligne.
"- Profession : " .$_POST['profession'].$passage_ligne.$passage_ligne;


//COTISATION
if ($passage_ligne == "<br>")
{ 	$message_html .="</div><div class=\"part\"><h2> COTISATION </h2>".$passage_ligne; }
else
{ 
	$message_html .=" ---------------".$passage_ligne.
	"/ COTISATION /".$passage_ligne.
	"---------------".$passage_ligne;
}
if ($_POST['cotisation']=="cotisation_a_jour")
{
	$message_html .="- Cotisation : Je suis à jour de cotisation pour cette année".$passage_ligne;
}
else
{
	$message_html .="- Cotisation : Je vais régler ma cotisation";
	if ($_POST['cotisation_type']=="cotisation_etud")
	{
		$message_html .=" au tarif étudiant réduit sans emploi : ".hachage_prix($prix_cotisation_etud)."€".$passage_ligne."- Abonnement de la revue SERVIR sous forme numérique.".$passage_ligne;
	}
	else
	{
		if ($_POST['cotisation_type']=="cotisation_foyer" and isset($_POST['cotisation_foyer_liste']))
		{
			$message_html .=": ".hachage_prix($prix_cotisation_foyer)."€"." , valable également pour les personnes ".$_POST['cotisation_foyer_liste']." habitant à la même adresse que moi";
		}
		else
		{
			$message_html .=" : ".hachage_prix($prix_cotisation_solo)."€";
		}
		$message_html .=$passage_ligne."- Abonnement revue SERVIR :";
		if (!isset($_POST['refus_abonnement_servir']))
		{
			$message_html .=" Compris ".hachage_prix($prix_abo_servir)."€".$passage_ligne;
		}
		else
		{
			$message_html .=" Non désiré.".$passage_ligne;
		}

	}
	
}

if ($passage_ligne == "<br>")
{ 	$message_html .="</div><div class=\"part\"><h2> PARTICIPATIONS </h2>".$passage_ligne; }
else
{ 
	$message_html .=" -----------------".$passage_ligne.
	"/ PARTICIPATIONS /".$passage_ligne.
	"-----------------".$passage_ligne;
}

if ($_POST['participation']=="participation_oui")
{
	$message_html .= "- Première Participation".$passage_ligne;

	if (isset($_POST['parrain_prenom']))
	{
		$message_html .= "*  Parrain(e) : ".$_POST['parrain_prenom']." ".$_POST['parrain_nom'].$passage_ligne;
	}
}

else
{
	$message_html .= "- A déjà participé à un pèlerinage".$passage_ligne;

	if ($_POST['participation_engagement']=="engagement_oui")
	{
	$message_html .= "*  Engagement réalisé".$passage_ligne;
	}
	else if (isset($_POST['participation_annees']))
	{$message_html .="*  Participations précédentes : ".$_POST['participation_annees'].$passage_ligne;}
}

/*
if (isset($_POST['rmq_participation']))
{$message_html .= "Remarque : ".$_POST['rmq_participation'].$passage_ligne;}
$message_html .=$passage_ligne;*/


if ($passage_ligne == "<br>")
{ 	$message_html .="</div><div class=\"part\"><h2> TRANSPORT </h2>".$passage_ligne; }
else
{ 
	$message_html .=" ------------".$passage_ligne.
	"/ TRANSPORT /".$passage_ligne.
	"------------".$passage_ligne;
}

if ($_POST['bus']=="bus_oui")
{
	$message_html .= "- Bus au départ de : ".$_POST['bus_depart'].$passage_ligne;
}

else
{$message_html .= "- Je ne prends pas le bus".$passage_ligne;}

if ($_POST['categorie']!="categorie_non")
{
	if ($_POST['categorie']=="categorie_couple")
	{
		$message_html .= "- Catégorie COUPLE : Je voyage en couple avec : ".$_POST['couple_prenom']." ".$_POST['couple_nom'].$passage_ligne.
		"*  Tarif choisis : ";
		if ($_POST['couple_tarif']=="couple_tarif_soutien")
			{$message_html .="Tarif soutien : ".hachage_prix($prix_couple_soutien)."€".$passage_ligne;}
		elseif ($_POST['couple_tarif']=="couple_tarif_reduit")
			{$message_html .="Tarif réduit : ".hachage_prix($prix_couple_reduit)."€".$passage_ligne;}
		
		if (isset($_POST['couple_paiement_fait']))
		{
			$message_html .= $passage_ligne."- Paiement déjà réalisé par mon/ma conjoint/te." .$passage_ligne;
		}

	}
	elseif ($_POST['categorie']=="categorie_jeune" and isset($_POST['categorie_promo_jeune']) )
	{
		$message_html .= "- PROMO JEUNE : Vous bénéficiez de la promo jeune : ".hachage_prix($prix_jeune)."€ (transport et hébergement pension complète compris)".$passage_ligne;
	}
	elseif ($_POST['categorie']=="categorie_ado" )
	{
		$message_html .= "- Catégorie ADO : Vous bénéficiez d'un tarif réduit : ".hachage_prix($prix_ado)."€".$passage_ligne;
	}
	elseif ($_POST['categorie']=="categorie_enfant" )
	{
		$message_html .= "- Catégorie ENFANT : Vous bénéficiez d'un tarif réduit : ".hachage_prix($prix_enfant)."€".$passage_ligne;
	}
}
elseif ($_POST['categorie']=="categorie_non" and $_POST['bus']=="bus_oui")
{
	$message_html .= "Je vais régler le transport : ".hachage_prix($prix_normal)."€".$passage_ligne;
}

if (isset($_POST['rmq_transport']))
{$message_html .= "Remarque : ".$_POST['rmq_transport'].$passage_ligne;}
$message_html .=$passage_ligne;


if ($passage_ligne == "<br>")
{ 	$message_html .="</div><div class=\"part\"><h2> HEBERGEMENT </h2>".$passage_ligne; }
else
{ 
	$message_html .=" --------------".$passage_ligne.
	"/ HEBERGEMENT /".$passage_ligne.
	"--------------".$passage_ligne;
}

$message_html.="- Hôtel : ".$_POST['hotel'].$passage_ligne.
"*  Type de chambre : ";

if (in_array($_POST['hotel'],$liste_hotel_ch_double))
{
	$message_html .= $_POST['hotel_chambre_12'].$passage_ligne;
}
if (in_array($_POST['hotel'],$liste_hotel_ch_triple))
{
	$message_html .= $_POST['hotel_chambre_123'].$passage_ligne;
}

if (!isset($_POST['categorie_promo_jeune']) and in_array($_POST['hotel'],$liste_hotel_paiement_online) )

{
	//prix Hebergement ave MARIA chambre simple
	if ($_POST['hotel']=="hotel_ave_maria" and $_POST['hotel_chambre_123']=="hotel_simple")
	{
		$message_html .="Je vais régler l'hébergement en pension complète".hachage_prix($prix_ave_maria_ch1)."€".$passage_ligne;
	}
	//prix Hebergement ave MARIA chambre double et triple
	if ($_POST['hotel']=="hotel_ave_maria" and ($_POST['hotel_chambre_123']=="hotel_double" or $_POST['hotel_chambre_123']=="hotel_triple"))
	{
		$message_html .="Je vais régler l'hébergement en pension complète".hachage_prix($prix_ave_maria_ch23)."€".$passage_ligne;
	}
	//prix Hebergement SAINT FRAI toute chambre
	if ($_POST['hotel']=="hotel_saint_frai")
	{
		$message_html .="Je vais régler l'hébergement en pension complète".hachage_prix($prix_saint_frai)."€".$passage_ligne;
	}

	//prix Hebergement SAINT MICHEL
	if ($_POST['hotel']=="hotel_saint_michel")
	{
		$message_html .="Je vais régler l'hébergement en pension complète".hachage_prix($prix_saint_michel)."€".$passage_ligne;
	}
}
elseif($_POST['hotel']!='hebergement_perso')
{
	$message_html .="Je règlerai l'hôtel sur place".$passage_ligne;
}

if (isset($_POST['chambre_prenom1']))
{
	$message_html .= "*  Je partage la chambre avec ".$_POST['chambre_prenom1']." ".$_POST['chambre_nom1']." ".$passage_ligne;
}
if (isset($_POST['chambre_prenom2']))
{
	$message_html .= "*  et ".$_POST['chambre_prenom2']." ".$_POST['chambre_nom2'];
}
if (isset($_POST['hotel_bis']))
{
	$message_html .= $passage_ligne."- Mon second choix d'hôtel est : ".$_POST['hotel_bis'].$passage_ligne."*  Type de chambre : ";
}
/*
if (in_array($_POST['hotel_bis'],$liste_hotel_ch_double))
{
	$message_html .= $_POST['hotel_chambre_12'].$passage_ligne;
}
if (in_array($_POST['hotel_bis'],$liste_hotel_ch_triple))
{
	$message_html .= $_POST['hotel_chambre_123'].$passage_ligne;
}
*/
if (!isset($_POST['cotisation_ave_maria']) and ($_POST['sexe']=="femme")and ($_POST['hotel']=="hotel_ave_maria"))
{
	$message_html .= "*  Je cotise 3€ pour l'Ave Maria".$passage_ligne;
}

if (isset($_POST['cotisation_ave_maria']) and ($_POST['sexe']=="femme"))
{
	$message_html .= "*  ".$_POST['nom_ave_maria']. ", membre de ma famille a déjà cotisé cette année".$passage_ligne;
}

if (isset($_POST['rmq_hebergement']))
{$message_html .= "Remarque : ".$_POST['rmq_hebergement'].$passage_ligne;}
$message_html .=$passage_ligne;


if ($passage_ligne == "<br>")
{ 	$message_html .="</div><div class=\"part\"><h2> SERVICE </h2>".$passage_ligne; }
else
{ 
	$message_html .=" ----------".$passage_ligne.
	"/ SERVICE /".$passage_ligne.
	"----------".$passage_ligne;
}


if (isset($_POST['service']))
{
	$message_html .= "- Je souhaite effectuer le service : ".$_POST['service'];
	if (isset($_POST['service_unite_toilette']))
	{
		$message_html .= $passage_ligne."*  Accepte d'aider à la toilette des personnes malades";
	}
	if (isset($_POST['service_garde']))
	{
		if ($_POST['service_garde'] == "service_garde_entiere")
		{
			$message_html .= $passage_ligne."*  Accepte de réaliser une nuit de garde entière";
		}
		if ($_POST['service_garde'] == "service_garde_demi")
		{
			$message_html .= $passage_ligne."*  Accepte de réaliser une demi nuit de garde (21h-1h30 ou 1h30-6h)";
		}
	}
	if (isset($_POST['service_piscine']))
	{
		$message_html .= $passage_ligne."- Service Piscine : Je me mets à la disposition de l'hospitalité Notre-Dame de Lourdes pendant une ou plusieurs demi-journées : ";
	}

	/*
	if (isset($_POST['service_piscine_']))
	{
		$message_html .= $_POST['service_piscine_'];
	}*/
	
	

}
/*
if (isset($_POST['rmq_service']))
{$message_html .=$passage_ligne ."Remarque : ".$_POST['rmq_service'].$passage_ligne;}
$message_html .=$passage_ligne;*/

if ($passage_ligne == "<br>")
{ 	$message_html .=$passage_ligne."</div><div class=\"part\"><h2> PROTOCOLE SANITAIRE </h2>".$passage_ligne; }
else
{ 
	$message_html .= $passage_ligne." ----------------------".$passage_ligne.
	"/ PROTOCOLE SANITAIRE /".$passage_ligne.
	"---------------------".$passage_ligne;
}

if (isset($_POST['protocole']))
	{
		$message_html .= "- J'accepte sans réserve toutes les conditions inscrites dans le protocole sanitaire".$passage_ligne;
	}


if ($passage_ligne == "<br>")
{ 	$message_html .=$passage_ligne."</div><div class=\"part\"><h2> FINALISATION </h2>".$passage_ligne; }
else
{ 
	$message_html .= $passage_ligne." ---------------".$passage_ligne.
	"/ FINALISATION /".$passage_ligne.
	"---------------".$passage_ligne;
}


if (isset($_POST['prise_de_vue']))
{
	$message_html .= "- J'accepte les conditions de prise de vue".$passage_ligne;
}



?>