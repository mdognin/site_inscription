<?php
//Suppression des balise PHP et HTML dans les champs

foreach ($liste_variable_bdd as $val)
{
	if (isset($_POST[$val]))
	{
		$_POST[$val]=strip_tags($_POST[$val]);
		$_POST[$val]=stripslashes($_POST[$val]);
		//Evite d'avoir des saut à la ligne dans les champs SQL, ce qui crée des erreurs
		$order = array("\r\n", "\n", "\r");
		$_POST[$val]=str_replace($order, " ",$_POST[$val]); 
		
		
		if ($_POST[$val]=="")
		{
			unset($_POST[$val]);
		}
	}
}
?>