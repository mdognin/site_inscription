# Site_inscription

Le site est accessible en suivant ce lien : https://inscription.hospitalite30.fr/demo_formulaire/formulaire.php

## Arborescence des dossiers sur le serveur

Ce site d'inscription héberge plusieurs formulaires d'inscriptions basées sur le même modèle.
Ici est présenté le code de la page de démo

Lorsqu'une personne charge le formulaire d'inscription, il ouvre les pages suivantes :
- formulaire.php : Première page où se trouve le formulaire
- formulaire_page2.php : Page récapitulatif de l'inscription avant redirection vers le terminal de paiement de la Caisse d'Epargne

Voici une capture d'écran montrant l'arborescence des formulaires avec les dossiers communs "texte" et "style"

![image.png](./image.png)

## Descriptifs des fichiers et dossier

Les pages HTML appelées par la personne sont les 2 fichiers suivants
- formulaire.php : Première page où se trouve le formulaire
- formulaire_page2.php : Page récapitulatif de l'inscription avant redirection vers le terminal de paiement de la Caisse d'Epargne

Le dossier "script" comprend différents scripts contenant des fonctions spécifiques de l'analyse du formulaire
- calcul_prix.php permet de calculer le prix de l'inscription
- definition_pele.php permet de recenser toute les informations à mettre à jour chaque année pour simplifier la maj du formulaire
- message_recapitulatif.php permet d'écrire le récapitulatif de l'inscription qui apparait 
    - En page 2 du formulaire
    - Dans le mail automatique envoyé après le paiement
- suppression_erreurs.php permet de supprimer certaines erreurs qui pourraient être entrées par la personne
- transaction.php permet de préparer les informations à envoyer au site de la banque
- validation.php permet de réaliser quelques vérifications/mise en forme sur le formulaire

Le dossier "texte" comporte tout les scripts communs à tout les différents formulaire d'inscritpions pour les différents évènements de l'année. Le but est de mutualiser un maximum de scripts pour simplifier les modification/améliorations. 
Pour des questions de sécurité ce dossier n'est accessible seulement depuis le serveur
- cle_test.txt stocke la clé permettant de communiquer avec le terminal de paiement de la banque
- compteur_transaction.php permet de générer un numéro unique pour chaque transaction
- compteur_transaction.txt mémorise le numéro de la dernière transaction réalisée
- insertion_bdd.php est le script permettant d'insérer les détails de l'inscritpion dans la bdd
- preparation_requete_bdd.php permet de préparer la requete SQL pour la bdd
- secure.php permet d'éviter qu'une requête SQL ou une erreur soit glissée dans un champs du formulaire
- statut_paiement_bdd.php permet de mettre à jour la bdd une fois la transaction finalisée sur le site de la banque. Inscription du statut du paiement "accepté" ou "refusé"

Le dossier "style" permet de centraliser tous les fichiers utiles à l'affichage des formulaires d'inscriptions
- Logo_hospitalite.png et font_formulaire.jpg sont les fichiers images du logo et du fond du formulaire
- favicon.png est le favicon
- style.css est le fichier centralisant toutes les instructions css pour l'affichage graphique du formulaire
- Les 3 fichiers javascripts permettent l'affichage dynamique du formulaire : affichage de certains champs en fonction des options choisis, vérification en temps réel que les options choisis correspondent à l'âge de l'inscrit

Les dossiers "mail" et "requete" permettent de stocker les textes des mails envoyés aux inscrits et les requetes envoyées au serveur en cas de panne de la base de donnée
