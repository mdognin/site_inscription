var hotel_ch_double = ['hotel_sainte_agnes','hotel_ave_maria','hotel_saint_frai']; //'hotel_croix_des_bretons','hotel_national_eco'
var hotel_ch_triple = ['hotel_national','hotel_stella_matutina','hotel_croix_des_bretons','hotel_croix_des_bretons_eco','hotel_stella_matutina_eco','hotel_national_eco','hotel_saint_frai','hotel_ariane'];

       function effacer(cible1,cible2)
       {
          document.forms["general"].elements[cible1].value="";
          document.forms["general"].elements[cible2].value="";
        
        }



        var elt_part= document.getElementById('participation'),
        elt_unit= document.getElementById('service_unite'),
        elt_coup= document.getElementById('categorie'),
       
        elt_promo_jeun= document.getElementById('categorie_promo_jeune'),        
        elt_pisc= document.getElementById('service_piscine');
        elt_cotis= document.getElementById('cotisation');
        elt_cotis_type= document.getElementById('cotisation_type');

        //SEXE
        elt_sexe= document.getElementById('sexe');
        elt_sexe.addEventListener('change',function(){
        val_sexe= elt_sexe.value;
        if (val_sexe=="femme")
        {
            sexe='femme';
        }
        else if (val_sexe=="homme")
        {
            sexe='homme';
        }
        },true);

        //AGE
        elt_age=document.getElementsByName('date_naissance')[0];
        date_depart = document.getElementById('date_depart');
        elt_age.addEventListener('change',function(){
            val_date_depart=date_depart.value;
            val_elt_age=elt_age.value;


            age=update_age(val_date_depart);
            },true);


        //BUS
        elt_bus= document.getElementById('bus');
        elt_bus.addEventListener('change',function(){
        val_bus= elt_bus.value;
        if (val_bus=="bus_oui")
        {
            document.getElementById('id_bus_oui').style.display='block';
            document.getElementById('id_bus_non').style.display='none';
        }
        else if(val_bus=="bus_non")
        {
            document.getElementById('id_bus_oui').style.display='none';
            document.getElementById('id_bus_non').style.display='block';
        }
        else 
        {
            document.getElementById('id_bus_oui').style.display='none';
            document.getElementById('id_bus_non').style.display='none';
        }
        },true);

        //CATEGORIE
        elt_cat= document.getElementById('categorie');
        
        elt_cat.addEventListener('change',function(){
            val_cat= elt_cat.value;
            update_cat(age);
            },true);

        //COTISATION // ABONNEMENT
        elt_cotis.addEventListener('change',function(){
        val_cotis= elt_cotis.value;
        if (val_cotis=="cotisation_a_regler")
        {
            document.getElementById('id_cotisation_type').style.display='block';
            document.getElementById('id_cotisation_abo').style.display='block';
            document.getElementById('id_note_2021').style.display='block';
            document.getElementById('test').style.backgroundColor='green';
        }
        else
        {
            document.getElementById('id_cotisation_type').style.display='none';
            document.getElementById('id_cotisation_foyer').style.display='none';
            document.getElementById('id_cotisation_abo').style.display='none';
            document.getElementById('id_note_2021').style.display='none';
            document.getElementById('id_cotisation_type').required = false;
        }
        },true);

        //COTISATION FOYER // ABONNEMENT
        elt_cotis_type.addEventListener('change',function(){
        val_cotis_type= elt_cotis_type.value;

        if (val_cotis_type=="cotisation_foyer")
        {
            document.getElementById('id_cotisation_foyer').style.display='block';
        }
        else
        {
            document.getElementById('id_cotisation_foyer').style.display='none';
        }
        },true);

        //PROMO JEUNE
        elt_promo_jeun.addEventListener('change',function(){
        val_promo_jeun= elt_promo_jeun.value;
        if (categorie_promo_jeune.checked)
        {
            document.getElementById('id_hebergement').style.display='none';
            document.getElementById('bloc_alerte_chambre').style.display='none';
            document.getElementById('hotel').required = false;
            if (sexe=='femme')
            {
                document.getElementById('id_cotisation_ave_maria').style.display='block';
            }
            else
            {
                document.getElementById('id_cotisation_ave_maria').style.display='none';
            }
        }
        else
        {
            document.getElementById('id_hebergement').style.display='block';
            document.getElementById('bloc_alerte_chambre').style.display='block';
            document.getElementById('id_cotisation_ave_maria').style.display='none';
        }
        },true);

        //COUPLE
        elt_coup.addEventListener('change',function(){
        val_coup= elt_coup.value;
        if (val_coup=="categorie_couple")
        {
            document.getElementById('id_categorie_couple').style.display='block';
        }
        else
        {
            document.getElementById('id_categorie_couple').style.display='none';
        }
        },true);

        //PARTICIPATION
        elt_part.addEventListener('change',function(){
        val_part= elt_part.value;
        if (val_part=="participation_oui")
        {
            document.getElementById('id_participation_oui').style.display='block';
            document.getElementById('id_participation_non').style.display='none';
        }
        else if(val_part=="participation_non")
        {
            document.getElementById('id_participation_oui').style.display='none';
            document.getElementById('id_participation_non').style.display='block';
        }
        else 
        {
            document.getElementById('id_participation_oui').style.display='none';
            document.getElementById('id_participation_non').style.display='none';
        }
        },true);

       

        //HOTEL
        
        //elt_hot_bis= document.getElementById('hotel_bis');
        elt_hot= document.getElementById('hotel');
        elt_hot.addEventListener('change',function(){
        val_hot= elt_hot.value;

        if (val_hot=="hotel_ave_maria")
        {
            document.getElementById('id_cotisation_ave_maria').style.display='block';
        }
        else 
        {
            document.getElementById('id_cotisation_ave_maria').style.display='none';
        }

        if(hotel_ch_double.includes(val_hot))
        {
            document.getElementById('id_hotel_ch_double').style.display = 'block';
            document.getElementById('id_hotel_ch_triple').style.display = 'none';
                         
        }
        else if (hotel_ch_triple.includes(val_hot))
        {
            document.getElementById('id_hotel_ch_double').style.display = 'none';
            document.getElementById('id_hotel_ch_triple').style.display = 'block';
        }
        else if ((val_hot=="hebergement_perso")||(val_hot==""))
        {
            document.getElementById('id_hotel_ch_double').style.display = 'none';
            document.getElementById('id_hotel_ch_triple').style.display = 'none';
            document.getElementById('bloc_alerte_chambre').style.display = 'none'; 

        }
        },true);


        //ALERTE PARTAGE CHAMBRE




        elt_hot_alerte_23=document.getElementById('hotel_chambre_123');
        elt_hot_alerte_12=document.getElementById('hotel_chambre_12');

        elt_hot_alerte_23.addEventListener('change',function(){
            val_hot_alerte_ch=elt_hot_alerte_23.value;
            update_hotel_ch();

            },true);



        elt_hot_alerte_12.addEventListener('change',function(){
            val_hot_alerte_ch=elt_hot_alerte_12.value;
            update_hotel_ch();
            },true);



        //SERVICE UNITE : CHECKBOX
        elt_unit.addEventListener('change',function(){
        if (elt_unit.checked)
        {
            document.getElementById('id_service_unite_toilette').style.display='block';
        }
        else 
        {
            document.getElementById('id_service_unite_toilette').style.display='none';
        }
        },true);


        //SERVICE PISCINE : CHECKBOX
        elt_pisc.addEventListener('change',function(){
        if (elt_pisc.checked)
        {
            document.getElementById('id_service_piscine_dispo').style.display='block';
        }
        else 
        {
            document.getElementById('id_service_piscine_dispo').style.display='none';
        }
        },true);

        
      
        //////////////////////


        function test_etat()
        {
            elt_test3= document.getElementById('equipe_med');
            val_test3= elt_test3.value;
            if (val_test3 == "medecin")
            {
                document.getElementById('test4').style.backgroundColor='green';
            }
            else
            {
                document.getElementById('test4').style.backgroundColor='red';
            }
        }

        elt_test3= document.getElementById('equipe_med');
        elt_test3.addEventListener('change',function(){test_etat();},true);*/



       

