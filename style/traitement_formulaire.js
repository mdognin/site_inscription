function update_hotel_ch()
{
if (val_hot_alerte_ch == "hotel_triple")
{
    document.getElementById('bloc_alerte_chambre').style.display='block';
    document.getElementById('personne2').style.display='block';
   
}
else if (val_hot_alerte_ch == "hotel_double")
{
    document.getElementById('bloc_alerte_chambre').style.display='block';
    document.getElementById('personne2').style.display='none';
}
else if(val_hot_alerte_ch == "hotel_simple")
{
    document.getElementById('bloc_alerte_chambre').style.display='none';
    document.getElementById('personne2').style.display='none';
}
}

function update_age(date_depart)
{

	var val_date_depart=date_depart.split('/');
	new_date_depart= new Date(val_date_depart[2],val_date_depart[1],val_date_depart[0]);
	var val_date_naissance= val_elt_age.split('-');
    dateu= new Date(val_date_naissance[0],val_date_naissance[1],val_date_naissance[2]);
    var diff = new_date_depart.getTime() - dateu.getTime();
    var diff = new Date(diff); 
    var age= Math.abs(diff.getUTCFullYear() - 1970);
    return age;
}

function update_cat(age)
{

    if ((val_cat!="categorie_jeune")&&(val_cat!="categorie_ado")&&(val_cat!="categorie_enfant"))
    {
        document.getElementById('bloc_alerte_age').style.display='none';
    }

    //ENFANT
    if (val_cat=="categorie_enfant")
    {
        if (age >10)
        {
            message="Votre âge de "+age+" ans ne correspond pas à la catégorie ENFANT";
            document.getElementById('bloc_alerte_age').style.display='block';
            alert(message);
        }  
        else
        {
            document.getElementById('bloc_alerte_age').style.display='none';
        }
    }
    else
    {
        
    }
    //ADO
    if (val_cat=="categorie_ado")
    {
        if (age >17)
        {
            message="Votre âge de "+age+" ans ne correspond pas à la catégorie ADO";
            document.getElementById('bloc_alerte_age').style.display='block';
            alert(message);
        } 
        else
        {
            document.getElementById('bloc_alerte_age').style.display='none';
        }
    }
    else
    {
    }
     
    //JEUNE
    if (val_cat=="categorie_jeune")
    {
        if (age >25)
        {
            message="Votre âge de "+age+" ans ne correspond pas à la catégorie ENFANT";
            document.getElementById('bloc_alerte_age').style.display='block';
            alert(message);
        } 
        else
        {
            document.getElementById('bloc_alerte_age').style.display='none';
        }
        document.getElementById('id_categorie_jeune').style.display='block';
    }

    else
    {
        document.getElementById('id_categorie_jeune').style.display='none';
        document.getElementById('id_hebergement').style.display='block';
        document.getElementById('id_hebergement_partage').style.display='block';
    }
    
        //COUPLE
    if (val_cat=="categorie_couple")
    {
        document.getElementById('id_categorie_couple').style.display='block';
    }
    else
    {
        document.getElementById('id_categorie_couple').style.display='none';
    }
}
