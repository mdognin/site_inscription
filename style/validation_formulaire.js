function fond_rouge(elt_cible)
{
   val_cible=elt_cible.value;
   if (val_cible == "")
   {
      elt_cible.style.backgroundColor='rgba(255,94,77,1)';//rouge vif
   }
   else
   {
      elt_cible.style.backgroundColor='rgba(176,242,182,1)';//vert
   }
}

elt_chambre_prenom1 = document.getElementsByName('chambre_prenom1')[0];
elt_chambre_prenom2= document.getElementsByName('chambre_prenom2')[0];
elt_chambre_nom1= document.getElementsByName('chambre_nom1')[0];
elt_chambre_nom2= document.getElementsByName('chambre_nom2')[0];

elt_chambre_prenom1.addEventListener('change',function(){fond_rouge(elt_chambre_prenom1);},true);
elt_chambre_nom1.addEventListener('change',function(){fond_rouge(elt_chambre_nom1);},true);
elt_chambre_prenom2.addEventListener('change',function(){fond_rouge(elt_chambre_prenom2);},true);
elt_chambre_nom2.addEventListener('change',function(){fond_rouge(elt_chambre_nom2);},true);
 
